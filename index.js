const app = require('express')()
const PORT = process.env.PORT ?? 4000

app.get('/', (req, res) => {
  res.send({"message": "DedopyatRygaetsa"})
})

app.listen(PORT, () => {
  console.log(`Application started on ${PORT} port`)
})


